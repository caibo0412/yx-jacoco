import Vue from 'vue';
// 导入创建的请求对象
import request from '@/api/request_api';
import getRequest from '@/api/get_request';
import formPostRequest from '@/api/form_post_request';
// eslint-disable-next-line camelcase
import get_form_request from '@/api/get_form_request';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import dataV from '@jiaminghi/data-view';
import App from './App.vue';
import router from './router';
import store from './store';

// 将请求对象绑定到Vue的原型上,这样全局都可以使用
Vue.prototype.$request = request;
Vue.prototype.$getRequest = getRequest;
Vue.prototype.$formPostRequest = formPostRequest;
// eslint-disable-next-line camelcase
Vue.prototype.$getFormRequest = get_form_request;
Vue.use(dataV);
Vue.use(ElementUI);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
