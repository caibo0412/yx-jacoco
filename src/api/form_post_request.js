import axios from 'axios';

const formPostRequest = axios.create({
  baseURL: ' http://10.9.50.219:18082',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'access-control-allow-origin': '*',
  },
});

export default formPostRequest;
