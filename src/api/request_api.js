import axios from 'axios';

const request = axios.create({
  baseURL: ' http://10.9.50.219:18082',
  headers: {
    'Content-Type': 'application/json',
    'access-control-allow-origin': '*',
  },
});

export default request;
