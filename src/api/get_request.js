import axios from 'axios';

const getRequest = axios.create({
  baseURL: 'http://10.9.50.219:18082',
  headers: {
    Accept: 'application/json, text/plain, */*',
    'access-control-allow-origin': '*',
  },
});

export default getRequest;
